package com.weather.jonathan.weatherapplication.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.weather.jonathan.weatherapplication.R;
import com.weather.jonathan.weatherapplication.adapters.GenericRecyclerViewAdapter;
import com.weather.jonathan.weatherapplication.adapters.view_holder.CityWeatherViewHolder;
import com.weather.jonathan.weatherapplication.data.WeatherData;
import com.weather.jonathan.weatherapplication.global.Constants;
import com.weather.jonathan.weatherapplication.interfaces.MainActivityView;
import com.weather.jonathan.weatherapplication.models.CitiesWeatherResponse;
import com.weather.jonathan.weatherapplication.models.Weather;
import com.weather.jonathan.weatherapplication.utils.AlertDialogUtils;
import com.weather.jonathan.weatherapplication.utils.AppUtils;
import com.weather.jonathan.weatherapplication.utils.LocationUtils;
import com.weather.jonathan.weatherapplication.utils.WeatherUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MainActivityView, LocationListener {

    @BindView(R.id.toolbar) protected Toolbar mToolbar;
    @BindView(R.id.drawer_layout) protected DrawerLayout mDrawer;
    @BindView(R.id.nav_view) protected NavigationView mNavigationView;
    @BindView(R.id.cities_rv) protected RecyclerView mCitiesRecyclerView;

    @Nullable @BindView(R.id.map_iv) protected ImageView mMapImageView;
    @Nullable @BindView(R.id.map_tv) protected TextView mMapTextView;
    @Nullable @BindView(R.id.city_tv) protected TextView mCityTextView;
    @Nullable @BindView(R.id.weather_tv) protected TextView mWeatherTextView;
    @Nullable @BindView(R.id.temperature_tv) protected TextView mTemperatureTextView;
    @Nullable @BindView(R.id.humidity_tv) protected TextView mHumidityTextView;
    @Nullable @BindView(R.id.wind_tv) protected TextView mWindTextView;

    @BindView(R.id.loadingLayout) protected LinearLayout loadingLayout;

    private List<Weather> mCitiesWeather = new ArrayList<>();
    private GenericRecyclerViewAdapter mRecyclerAdapter;
    private GoogleApiClient mGoogleApiClient;
    private boolean isTablet;
    private LocationRequest mLocationRequest;
    private Location mLocation;

    private WeatherData mWeatherData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initLeftMenuAndToolbar();

        isTablet = getResources().getBoolean(R.bool.isTablet);

        mWeatherData = new WeatherData(this);

        initializeRecyclerView(mCitiesWeather);
        checkForGoogleServices();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        mGoogleApiClient.disconnect();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.LOCATION_PERMISSIONS_REQUEST: {
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    checkLocationEnabled();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        AlertDialogUtils.showAlertDialog(this, getString(R.string.alert_location_permission_title),
                                getString(R.string.alert_location_permission_description), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSIONS_REQUEST);
                                        dialogInterface.dismiss();
                                    }
                                });
                    } else{
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSIONS_REQUEST);
                    }
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        checkLocationEnabled();
    }

    /**
     * Check for google services in the device
     */
    private void checkForGoogleServices(){
        final int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (status == ConnectionResult.SUCCESS){
            checkPermission();
        }else{
            Toast.makeText(this, getString(R.string.google_not_found), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Initialize left menu and toolbar
     */
    private void initLeftMenuAndToolbar(){
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * Checks for app permissions
     */
    public void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSIONS_REQUEST);
        } else{
            checkLocationEnabled();
        }
    }

    /**
     * Checks for location enabled
     */
    private void checkLocationEnabled(){
        if(LocationUtils.isLocationEnabled(this)){
            startGoogleApiConnection();
        }else{
            locationDialog();
        }
    }

    /**
     * Shows location dialog to the user to turn on the Location
     */
    private void locationDialog(){
        AlertDialogUtils.showAlertDialog(this, getString(R.string.alert_location_title), getString(R.string.alert_location_description),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, Constants.REQUEST_ENABLE_GPS);
                dialogInterface.dismiss();
            }
        });
    }

    /**
     * Calls the Weather web service
     * @param lat String latitude
     * @param lon String longitude
     */
    private void callWeatherService(String lat, String lon){
        boolean isAvailable = AppUtils.isTimeForAvailableRequest(this);
        if(isAvailable){
            mWeatherData.getWeatherCitiesList(lat, lon);
        }else{
            AlertDialogUtils.showAlertDialog(this, getString(R.string.alert_alert),
                    getString(R.string.alert_wait), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        }
    }

    @Override
    public void showProgressDialog(){
        loadingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorDialog(){
        AlertDialogUtils.showAlertDialog(this, getString(R.string.alert_error_title),
                getString(R.string.alert_error_description), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
    }

    @Override
    public void handleCitiesWeatherResponse(CitiesWeatherResponse citiesWeatherResponse){
        loadingLayout.setVisibility(View.GONE);
        if(citiesWeatherResponse != null && citiesWeatherResponse.getList() != null){
            mCitiesWeather.clear();
            mCitiesWeather.addAll(citiesWeatherResponse.getList());
            if(mCitiesWeather.isEmpty()){
                Toast.makeText(this, getString(R.string.cities_not_found), Toast.LENGTH_LONG).show();
            }
        }
        mRecyclerAdapter.notifyDataSetChanged();
    }

    /**
     * Init recycler view
     * @param items List<Weather> list of items
     */
    private void initializeRecyclerView(final List<Weather> items){
        mCitiesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerAdapter = new GenericRecyclerViewAdapter(items) {
            @Override
            public int onGetItemViewType(int position) {
                return 0;
            }

            @Override
            public RecyclerView.ViewHolder onViewHolderCreation(ViewGroup viewGroup, int viewType) {
                View view = LayoutInflater.from(getBaseContext())
                        .inflate(R.layout.card_view, viewGroup, false);
                CityWeatherViewHolder holder = new CityWeatherViewHolder(view);
                return holder;
            }

            @Override
            public void onViewHolderBinding(RecyclerView.ViewHolder viewHolder, int itemPosition) {
                final CityWeatherViewHolder holder = (CityWeatherViewHolder) viewHolder;
                holder.mItem = items.get(itemPosition);
                holder.cityTextView.setText(WeatherUtils.getCityNameWithCountry(holder.mItem.getName(), holder.mItem.getSys()));
                holder.temperatureTextView.setText(WeatherUtils.getCityTemperatureWithLabel(holder.mItem.getMain()));

                Picasso.with(MainActivity.this)
                        .load(WeatherUtils.getCityMap(holder.mItem.getCoord()))
                        .into(holder.mapImageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.mapTextView.setVisibility(View.INVISIBLE);
                            }

                            @Override
                            public void onError() {
                                holder.mapTextView.setText(getResources().getString(R.string.error_loading_map));
                                holder.mapTextView.setVisibility(View.VISIBLE);
                            }
                        });

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(isTablet){
                            updateDetailData(holder.mItem);
                        }else{
                            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                            intent.putExtra(Constants.WEATHER_INTENT, holder.mItem);
                            startActivity(intent);
                        }
                    }
                });
            }
        };

        mCitiesRecyclerView.setAdapter(mRecyclerAdapter);
    }

    /**
     * Updates description information for tablets
     * @param weather Weather object
     */
    private void updateDetailData(Weather weather){
        Picasso.with(this)
                .load(WeatherUtils.getCityMap(weather.getCoord()))
                .into(mMapImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        mMapTextView.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError() {
                        mMapTextView.setText(getResources().getString(R.string.error_loading_map));
                        mMapTextView.setVisibility(View.VISIBLE);
                    }
                });

        String cityName = WeatherUtils.getCityNameWithCountry(weather.getName(), weather.getSys());
        if(cityName.isEmpty()){
            mCityTextView.setText(getString(R.string.empty_data));
        }else{
            mCityTextView.setText(cityName);
        }

        String weatherDescription = WeatherUtils.getWeatherDescription(weather.getWeather());
        if(weatherDescription.isEmpty()){
            mWeatherTextView.setText(getString(R.string.empty_data));
        }else{
            mWeatherTextView.setText(weatherDescription);
        }

        String temperature = WeatherUtils.getCityTemperature(weather.getMain());
        if(temperature.isEmpty()){
            mTemperatureTextView.setText(getString(R.string.empty_data));
        }else{
            mTemperatureTextView.setText(temperature);
        }

        String humidity = WeatherUtils.getCityHumidity(weather.getMain());
        if(humidity.isEmpty()){
            mHumidityTextView.setText(getString(R.string.empty_data));
        }else{
            mHumidityTextView.setText(humidity);
        }

        String wind = WeatherUtils.getCityWind(weather.getWind());
        if(wind.isEmpty()){
            mWindTextView.setText(getString(R.string.empty_data));
        }else{
            mWindTextView.setText(wind);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            AlertDialogUtils.showAlertDialogForSettingsMenu(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_cities_list) {
            if(mLocation != null){
                String lat = String.valueOf(mLocation.getLatitude());
                String lon = String.valueOf(mLocation.getLongitude());
                callWeatherService(lat, lon);
            }else{
                Toast.makeText(this, getString(R.string.location_not_found), Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null){
            final String lat = String.valueOf(location.getLatitude());
            final String lon = String.valueOf(location.getLongitude());
            if(mLocation == null){
                mLocation = location;
                callWeatherService(lat, lon);
            }else{
                String oldLat = String.valueOf(mLocation.getLatitude());
                String oldLon = String.valueOf(mLocation.getLongitude());
                if((!oldLat.equals(lat) && (!oldLon.equals(lon)))){
                    AlertDialogUtils.showNewLocationAlertDialog(this, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            callWeatherService(lat, lon);
                        }
                    });
                }
            }
        } else{
            Toast.makeText(this, getString(R.string.location_not_found), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows connection fail alert
     */
    private void showConnectionFailAlert(){
        AlertDialogUtils.showFailConnectionAlertDialog(this, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startGoogleApiConnection();
            }
        });
    }

    /**
     * Connection with google API
     */
    private void startGoogleApiConnection() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnectionSuspended(int cause) {
                            showConnectionFailAlert();
                        }

                        @Override
                        public void onConnected(Bundle connectionHint) {
                            registerRequestUpdate(MainActivity.this);
                        }
                    }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult result) {
                            showConnectionFailAlert();
                        }
                    }).build();
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient.connect();
        }
    }

    /**
     * Register the request update for locations
     * @param listener LocationListener
     */
    private void registerRequestUpdate(final LocationListener listener) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(Constants.MIN_12); // every 12 min

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener);
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    registerRequestUpdate(listener);
                }
            }
        }, Constants.SEC_1);
    }

}

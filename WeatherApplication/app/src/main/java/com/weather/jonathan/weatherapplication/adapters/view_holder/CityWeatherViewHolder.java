package com.weather.jonathan.weatherapplication.adapters.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weather.jonathan.weatherapplication.R;
import com.weather.jonathan.weatherapplication.models.Weather;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class CityWeatherViewHolder extends RecyclerView.ViewHolder {

    public final View mView;
    @BindView(R.id.city_tv) public TextView cityTextView;
    @BindView(R.id.temperature_tv) public TextView temperatureTextView;
    @BindView(R.id.map_tv) public TextView mapTextView;
    @BindView(R.id.city_rl) public RelativeLayout cityRelativeLayout;
    @BindView(R.id.map_iv) public ImageView mapImageView;


    public Weather mItem;

    public CityWeatherViewHolder(View view) {
        super(view);
        mView = view;
        ButterKnife.bind(this, view);
    }

}

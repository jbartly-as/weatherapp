package com.weather.jonathan.weatherapplication.data;

import com.weather.jonathan.weatherapplication.global.Constants;
import com.weather.jonathan.weatherapplication.interfaces.WeatherService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class WeatherServiceHelper {

    private static WeatherService instance = null;

    public static WeatherService getService() {
        if(instance == null) {
            instance = getBaseService();
        }
        return instance;
    }

    /**
     * Get the service for Weather API
     * @return WeatherService
     */
    private static WeatherService getBaseService(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WeatherService service = retrofit.create(WeatherService.class);
        return service;
    }

}

package com.weather.jonathan.weatherapplication.interfaces;

import com.weather.jonathan.weatherapplication.models.CitiesWeatherResponse;

/**
 * Created by Jonathan on 31/10/2017.
 */

public interface MainActivityView {
    void handleCitiesWeatherResponse(CitiesWeatherResponse citiesWeatherResponse);
    void showProgressDialog();
    void showErrorDialog();
}

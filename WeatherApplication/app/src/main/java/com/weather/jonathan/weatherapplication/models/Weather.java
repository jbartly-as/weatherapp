package com.weather.jonathan.weatherapplication.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class Weather implements Serializable {

    private Coordinates coord;
    private Country sys;
    private List<WeatherDescription> weather;
    private MainWeather main;
    private Wind wind;
    private String name;

    public Coordinates getCoord() {
        return coord;
    }

    public void setCoord(Coordinates coord) {
        this.coord = coord;
    }

    public Country getSys() {
        return sys;
    }

    public void setSys(Country sys) {
        this.sys = sys;
    }

    public List<WeatherDescription> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherDescription> weather) {
        this.weather = weather;
    }

    public MainWeather getMain() {
        return main;
    }

    public void setMain(MainWeather main) {
        this.main = main;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

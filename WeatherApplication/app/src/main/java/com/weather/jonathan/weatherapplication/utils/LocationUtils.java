package com.weather.jonathan.weatherapplication.utils;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by jbartly-as on 11/1/17.
 */

public class LocationUtils {

    /**
     * Checks for GPS or WIFI Enabled
     * @param context Context
     * @return true if Enabled, false otherwise
     */
    public static boolean isLocationEnabled(Context context){
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gpsProvider = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean networkProvider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gpsProvider || networkProvider){
            return true;
        }else{
            return false;
        }
    }

}

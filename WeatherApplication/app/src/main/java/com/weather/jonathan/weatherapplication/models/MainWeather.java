package com.weather.jonathan.weatherapplication.models;

import java.io.Serializable;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class MainWeather implements Serializable {

    private String temp;
    private String humidity;

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
}

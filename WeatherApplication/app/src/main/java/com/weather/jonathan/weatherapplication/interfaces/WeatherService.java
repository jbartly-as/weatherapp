package com.weather.jonathan.weatherapplication.interfaces;

import com.weather.jonathan.weatherapplication.models.CitiesWeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jonathan on 31/10/2017.
 */

public interface WeatherService {

    @GET("find")
    Call<CitiesWeatherResponse> getCitiesWeather(@Query("lat") String lat,
                                                 @Query("lon") String lon,
                                                 @Query("units") String units,
                                                 @Query("APPID") String apiKey);

}

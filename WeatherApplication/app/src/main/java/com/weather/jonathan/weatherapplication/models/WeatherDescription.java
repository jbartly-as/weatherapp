package com.weather.jonathan.weatherapplication.models;

import java.io.Serializable;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class WeatherDescription implements Serializable {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.weather.jonathan.weatherapplication.data;

import com.weather.jonathan.weatherapplication.global.Constants;
import com.weather.jonathan.weatherapplication.interfaces.MainActivityView;
import com.weather.jonathan.weatherapplication.models.CitiesWeatherResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class WeatherData {

    private MainActivityView view;

    public WeatherData(MainActivityView mainActivityView){
        this.view = mainActivityView;
    }

    /**
     * Get the list of cities with the weather of each one
     * @param lat String latitude
     * @param lon String longitude
     */
    public void getWeatherCitiesList(String lat, String lon){
        view.showProgressDialog();
        String apiKey = Constants.API_KEY;
        Call<CitiesWeatherResponse> call = WeatherServiceHelper.getService()
                .getCitiesWeather(lat, lon, Constants.UNITS_METRIC, apiKey);
        call.enqueue(new Callback<CitiesWeatherResponse>() {
            @Override
            public void onResponse(Call<CitiesWeatherResponse> call, Response<CitiesWeatherResponse> response) {
                if(response.isSuccessful()){
                    view.handleCitiesWeatherResponse(response.body());
                }else{
                    view.showErrorDialog();
                }
            }

            @Override
            public void onFailure(Call<CitiesWeatherResponse> call, Throwable t) {
                view.showErrorDialog();
            }
        });
    }
}

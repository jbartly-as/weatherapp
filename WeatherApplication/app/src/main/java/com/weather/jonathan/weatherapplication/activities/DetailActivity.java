package com.weather.jonathan.weatherapplication.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.weather.jonathan.weatherapplication.R;
import com.weather.jonathan.weatherapplication.global.Constants;
import com.weather.jonathan.weatherapplication.models.Weather;
import com.weather.jonathan.weatherapplication.utils.WeatherUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.map_iv) protected ImageView mMapImageView;
    @BindView(R.id.map_tv) protected TextView mMapTextView;
    @BindView(R.id.city_tv) protected TextView mCityTextView;
    @BindView(R.id.weather_tv) protected TextView mWeatherTextView;
    @BindView(R.id.temperature_tv) protected TextView mTemperatureTextView;
    @BindView(R.id.humidity_tv) protected TextView mHumidityTextView;
    @BindView(R.id.wind_tv) protected TextView mWindTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        Weather weather = (Weather)getIntent().getSerializableExtra(Constants.WEATHER_INTENT);
        updateDetailData(weather);
    }

    /**
     * Updates description information for phones
     * @param weather Weather object
     */
    private void updateDetailData(Weather weather){
        Picasso.with(this)
                .load(WeatherUtils.getCityMap(weather.getCoord()))
                .into(mMapImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        mMapTextView.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onError() {
                        mMapTextView.setText(getResources().getString(R.string.error_loading_map));
                        mMapTextView.setVisibility(View.VISIBLE);
                    }
                });

        String cityName = WeatherUtils.getCityNameWithCountry(weather.getName(), weather.getSys());
        if(cityName.isEmpty()){
            mCityTextView.setText(getString(R.string.empty_data));
        }else{
            mCityTextView.setText(cityName);
        }

        String weatherDescription = WeatherUtils.getWeatherDescription(weather.getWeather());
        if(weatherDescription.isEmpty()){
            mWeatherTextView.setText(getString(R.string.empty_data));
        }else{
            mWeatherTextView.setText(weatherDescription);
        }

        String temperature = WeatherUtils.getCityTemperature(weather.getMain());
        if(temperature.isEmpty()){
            mTemperatureTextView.setText(getString(R.string.empty_data));
        }else{
            mTemperatureTextView.setText(temperature);
        }

        String humidity = WeatherUtils.getCityHumidity(weather.getMain());
        if(humidity.isEmpty()){
            mHumidityTextView.setText(getString(R.string.empty_data));
        }else{
            mHumidityTextView.setText(humidity);
        }

        String wind = WeatherUtils.getCityWind(weather.getWind());
        if(wind.isEmpty()){
            mWindTextView.setText(getString(R.string.empty_data));
        }else{
            mWindTextView.setText(wind);
        }
    }

}

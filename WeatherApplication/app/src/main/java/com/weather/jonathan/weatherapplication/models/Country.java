package com.weather.jonathan.weatherapplication.models;

import java.io.Serializable;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class Country implements Serializable {

    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

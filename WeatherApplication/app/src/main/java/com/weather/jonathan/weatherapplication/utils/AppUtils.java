package com.weather.jonathan.weatherapplication.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.weather.jonathan.weatherapplication.global.Constants;

import java.util.Calendar;

/**
 * Created by Jonathan on 2/11/2017.
 */

public class AppUtils {

    /**
     * Save the time(day, hour and minutes) in the shared preferences
     * @param context Context
     * @param day int
     * @param hour int
     * @param minute int
     */
    private static void saveTime(Context context, int day, int hour, int minute){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(Constants.SHARED_PREFERENCES_DAY, day);
        editor.putInt(Constants.SHARED_PREFERENCES_HOUR, hour);
        editor.putInt(Constants.SHARED_PREFERENCES_MINUTE, minute);
        editor.commit();
    }

    /**
     * Ask if has past 12 minutes in order to update the information
     * @param context Context
     * @return true if 10 min has past, false otherwise
     */
    public static boolean isTimeForAvailableRequest(Context context){
        boolean isAvailable;
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        int day = sharedPref.getInt(Constants.SHARED_PREFERENCES_DAY, -1);
        int hour = sharedPref.getInt(Constants.SHARED_PREFERENCES_HOUR, -1);
        int minutes = sharedPref.getInt(Constants.SHARED_PREFERENCES_MINUTE, -1);

        Calendar calendar = Calendar.getInstance();
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        int currentHour = calendar.get(Calendar.HOUR);
        int currentMinutes = calendar.get(Calendar.MINUTE);

        if((day == -1) && (hour == -1) && (minutes == -1)){
            isAvailable = true;
        }else{
            if((day != currentDay) || (hour != currentHour)){
                isAvailable = true;
            } else if((currentMinutes - minutes) >= Constants.MINUTES_12){
                isAvailable = true;
            } else{
                isAvailable = false;
            }
        }

        if(isAvailable){
            saveTime(context, currentDay, currentHour, currentMinutes);
        }

        return isAvailable;
    }

}

package com.weather.jonathan.weatherapplication.utils;

import com.weather.jonathan.weatherapplication.models.Coordinates;
import com.weather.jonathan.weatherapplication.models.Country;
import com.weather.jonathan.weatherapplication.models.MainWeather;
import com.weather.jonathan.weatherapplication.models.WeatherDescription;
import com.weather.jonathan.weatherapplication.models.Wind;

import java.util.List;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class WeatherUtils {

    /**
     * Get city name with the country name
     * @param cityName String city name
     * @param country Country object
     * @return String city full name
     */
    public static String getCityNameWithCountry(String cityName, Country country){
        String completeName = cityName;
        if((country != null) && (country.getCountry() != null) && (!country.getCountry().isEmpty())){
            completeName = completeName + ", " + country.getCountry();
        }
        return completeName;
    }

    /**
     * Get the city temperature with label
     * @param mainWeather MainWeather object
     * @return String temperature
     */
    public static String getCityTemperatureWithLabel(MainWeather mainWeather){
        String temperature = "Temperature: ";
        if((mainWeather != null) && (mainWeather.getTemp() != null) && (!mainWeather.getTemp().isEmpty())){
            temperature = temperature + mainWeather.getTemp() + " C°";
        }
        return temperature;
    }

    /**
     * Get the city temperature
     * @param mainWeather MainWeather object
     * @return String temperature
     */
    public static String getCityTemperature(MainWeather mainWeather){
        String temperature = "";
        if((mainWeather != null) && (mainWeather.getTemp() != null) && (!mainWeather.getTemp().isEmpty())){
            temperature = temperature + mainWeather.getTemp() + " C°";
        }
        return temperature;
    }

    /**
     * Get the city url map
     * @param coordinates Coordinates object
     * @return String city url map
     */
    public static String getCityMap(Coordinates coordinates){
        String map = "";
        if((coordinates != null) && (coordinates.getLat() != null) && (!coordinates.getLat().isEmpty())
                && (coordinates.getLon() != null) && (!coordinates.getLon().isEmpty())){
            map = "http://maps.google.com/maps/api/staticmap?center=" + coordinates.getLat() + "," + coordinates.getLon() + "&zoom=10&size=200x200&sensor=false";
        }
        return map;
    }

    /**
     * Get weather description from a list
     * @param weather List<WeatherDescription>
     * @return String weather description
     */
    public static String getWeatherDescription(List<WeatherDescription> weather){
        StringBuilder builder = new StringBuilder();
        if(weather != null){
            for(WeatherDescription description : weather){
                if((description != null) && (description.getDescription() != null) && (!description.getDescription().isEmpty())){
                    builder.append(description.getDescription());
                    builder.append(". ");
                }
            }
        }

        return builder.toString();
    }

    /**
     * Get the city humidity
     * @param mainWeather MainWeather object
     * @return String humidity
     */
    public static String getCityHumidity(MainWeather mainWeather){
        String humidity = "";
        if((mainWeather != null) && (mainWeather.getHumidity() != null) && (!mainWeather.getHumidity().isEmpty())){
            humidity = humidity + mainWeather.getHumidity() + "%";
        }
        return humidity;
    }

    /**
     * Get the city wind speed
     * @param wind Wind object
     * @return String wind speed
     */
    public static String getCityWind(Wind wind){
        String windSpeed = "";
        if((wind != null) && (wind.getSpeed() != null) && (!wind.getSpeed().isEmpty())){
            windSpeed = windSpeed + wind.getSpeed() + " meter/sec";
        }
        return windSpeed;
    }

}

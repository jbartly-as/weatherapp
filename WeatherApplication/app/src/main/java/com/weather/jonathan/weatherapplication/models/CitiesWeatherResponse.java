package com.weather.jonathan.weatherapplication.models;

import java.util.List;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class CitiesWeatherResponse {

    private String count;
    private List<Weather> list;

    public String getCount() {
        return count;
    }

    public void setCount(String cnt) {
        this.count = cnt;
    }

    public List<Weather> getList() {
        return list;
    }

    public void setList(List<Weather> list) {
        this.list = list;
    }
}

package com.weather.jonathan.weatherapplication.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.weather.jonathan.weatherapplication.R;

/**
 * Created by jbartly-as on 11/1/17.
 */

public class AlertDialogUtils {

    /**
     * Shows alert dialog when user tap settings option in the menu
     * @param context Context
     */
    public static void showAlertDialogForSettingsMenu(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.alert_title);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(R.string.alert_description);
        builder.setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Shows alert dialog with customized title, description and onClickListener
     * @param context Context
     * @param title String title
     * @param msg String message
     * @param okClickListener DialogInterface.OnClickListener
     */
    public static void showAlertDialog(Context context, String title, String msg, DialogInterface.OnClickListener okClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(msg);
        builder.setPositiveButton(context.getString(R.string.alert_ok), okClickListener);
        builder.show();
    }

    /**
     * Shows alert dialog when a new location is found
     * @param context Context
     * @param okClickListener DialogInterface.OnClickListener
     */
    public static void showNewLocationAlertDialog(Context context, DialogInterface.OnClickListener okClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.alert_location_title));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(context.getString(R.string.alert_location_found));
        builder.setPositiveButton(context.getString(R.string.alert_ok), okClickListener);
        builder.setNegativeButton(context.getString(R.string.alert_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Shows alert dialog when a google connection has failed
     * @param context Context
     * @param okClickListener DialogInterface.OnClickListener
     */
    public static void showFailConnectionAlertDialog(Context context, DialogInterface.OnClickListener okClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.alert_connection_title));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(context.getString(R.string.alert_connection_fail));
        builder.setPositiveButton(context.getString(R.string.alert_retry), okClickListener);
        builder.setNegativeButton(context.getString(R.string.alert_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

}

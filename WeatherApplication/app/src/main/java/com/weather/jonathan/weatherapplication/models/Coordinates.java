package com.weather.jonathan.weatherapplication.models;

import java.io.Serializable;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class Coordinates implements Serializable {

    private String lon;
    private String lat;

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}

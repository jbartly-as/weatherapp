package com.weather.jonathan.weatherapplication.global;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class Constants {

    public static final String API_KEY = "b9d307139f5cacfd62feb64030db5a3c";
    public static final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String UNITS_METRIC = "metric";
    public static final int REQUEST_ENABLE_GPS = 100;
    public static final int LOCATION_PERMISSIONS_REQUEST = 101;
    public static final String WEATHER_INTENT = "weatherIntent";
    public static final int MIN_12 = 720000;
    public static final int MINUTES_12 = 12;
    public static final int SEC_1 = 1000;
    public static final String SHARED_PREFERENCES = "weatherAppPreferences";
    public static final String SHARED_PREFERENCES_DAY = "day";
    public static final String SHARED_PREFERENCES_HOUR = "hour";
    public static final String SHARED_PREFERENCES_MINUTE = "minute";

}

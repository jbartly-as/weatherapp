package com.weather.jonathan.weatherapplication.models;

import java.io.Serializable;

/**
 * Created by Jonathan on 31/10/2017.
 */

public class Wind implements Serializable {

    private String speed;

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
